-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema boozt
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema boozt
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `boozt` DEFAULT CHARACTER SET utf8 ;
USE `boozt` ;

-- -----------------------------------------------------
-- Table `boozt`.`country`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `boozt`.`country` (
  `code` VARCHAR(2) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`code`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `boozt`.`customer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `boozt`.`customer` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(100) NOT NULL,
  `last_name` VARCHAR(100) NOT NULL,
  `email` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `boozt`.`order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `boozt`.`order` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `purchase_date` DATETIME NOT NULL,
  `device` VARCHAR(45) NULL,
  `country_code` VARCHAR(2) NOT NULL,
  `customer_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_order_country_idx` (`country_code` ASC),
  INDEX `fk_order_customer_idx` (`customer_id` ASC),
  INDEX `purchase_date` (`purchase_date` ASC),
  CONSTRAINT `fk_order_country`
    FOREIGN KEY (`country_code`)
    REFERENCES `boozt`.`country` (`code`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_customer1`
    FOREIGN KEY (`customer_id`)
    REFERENCES `boozt`.`customer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `boozt`.`order_item`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `boozt`.`order_item` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `ean` VARCHAR(13) NOT NULL,
  `quatity` DECIMAL(15,2) NOT NULL,
  `price` DECIMAL(15,2) NOT NULL,
  `order_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_order_item_order_idx` (`order_id` ASC),
  CONSTRAINT `fk_order_item_order1`
    FOREIGN KEY (`order_id`)
    REFERENCES `boozt`.`order` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
