# Boozt Dashboad

## Getting started
Clone repository:<br />
`git clone https://gitlab.com/victorlss/boozt-dashboard.git`<br />
Enter on directory:<br />
`cd boozt-dashboard`<br />
Run Docker commands:<br />
`docker-compose build`<br />
`docker-compose up -d`<br />

Waiting few seconds to setting up database...<br />
The app is running in http://localhost:8080/