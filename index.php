<?php
header('Content-Type: text/html; charset=utf-8');

require_once __DIR__ . '/vendor/autoload.php';

use \Core\Routing\Router;
use \Core\Routing\Request;

$router = new Router(new Request);

//Dashboard
$router->get('/', array(
    'controller' => 'Dashboard',
    'action' => 'index'
));

//About
$router->get('/About', array(
    'controller' => 'About',
    'action' => 'index'
));

//Order
$router->get('/Order/All', array(
    'controller' => 'Order',
    'action' => 'all'
));

$router->post('/Order/DataChart', array(
    'controller' => 'Order',
    'action' => 'dataChart'
));

$router->post('/Order/Count', array(
    'controller' => 'Order',
    'action' => 'count'
));

$router->post('/Order/Revenue', array(
    'controller' => 'Order',
    'action' => 'revenue'
));

//Customer
$router->post('/Customer/Count', array(
    'controller' => 'Customer',
    'action' => 'count'
));