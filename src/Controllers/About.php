<?php

namespace Controllers;

class About extends ControllerAbstract
{
    public function index()
    {
        $this->renderView('About');
    }
}