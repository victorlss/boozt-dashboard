<?php

namespace Controllers;

class Order extends ControllerAbstract
{
    public function all()
    {
        $dal = new \DAL\Order();
        $this->renderJson($dal->getAll());
    }
    public function count($data)
    {
        $startDate = $data['startDate'];
        $endDate = $data['endDate'];

        $dal = new \DAL\Order();
        $json['count'] = $dal->getCountByPeriod($startDate, $endDate);
        $this->renderJson($json);
    }

    public function revenue($data)
    {
        $startDate = $data['startDate'];
        $endDate = $data['endDate'];

        $dal = new \DAL\Order();
        $json['revenue'] = $dal->getRevenueByPeriod($startDate,$endDate);
        $this->renderJson($json);
    }

    public function dataChart($data)
    {
        $startDate = $data['startDate'];
        $endDate = $data['endDate'];

        $dal = new \DAL\Customer();
        $countCustomers = $dal->getCount();

        $dal = new \DAL\Order();
        $items = $dal->getCountDateByPeriod($startDate, $endDate);
        $json = array(
            'count' => array(),
            'categories' => array(),
            'customers' => array()
        );
        
        foreach ($items as $item) {
            array_push($json["count"], (int)$item['count']);
            array_push($json["categories"], $item['date']);
            array_push($json["customers"], $countCustomers);
        }

        $this->renderJson($json);
    }
}

