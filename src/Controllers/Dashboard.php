<?php

namespace Controllers;

class Dashboard extends ControllerAbstract
{
    public function index()
    {   
        $this->renderView("Dashboard");
    }
}
