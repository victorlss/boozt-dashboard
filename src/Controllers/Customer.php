<?php

namespace Controllers;

class Customer extends ControllerAbstract
{
    public function count($data)
    {
        $startDate = $data['startDate'];
        $endDate = $data['endDate'];

        $dal = new \DAL\Customer();
        $json['count'] = $dal->getCount();
        $this->renderJson($json);
    }
}