<?php

namespace Controllers;
use Core\Configuration;

abstract class ControllerAbstract
{
    protected $configuration;

    public function __construct(Configuration $configuration) {
        //Dependency Injection
        $this->configuration = $configuration;
    }

    protected function renderView($view)
    {
        header('Content-Type: text/html; charset=utf-8');
        include($this->configuration->baseDir . "/src/Views/_Layout/Header.php");
        include($this->configuration->baseDir . "/src/Views/$view.php");
        include($this->configuration->baseDir . "/src/Views/_Layout/Footer.php");
    }

    protected function renderJson($obj)
    {
        header("Content-type: application/json; charset=utf-8");
        echo json_encode($obj);
    }
}