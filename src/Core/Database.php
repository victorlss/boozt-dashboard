<?php

namespace Core;

class Database
{
    public $connection;

    function __construct()
    {
        $configuration = new Configuration();
        $drive = $configuration->database['drive'];
        $host = $configuration->database['host'];
        $dbname = $configuration->database['dbname'];
        $user = $configuration->database['user'];
        $password = $configuration->database['password'];

        $this->connection = new \PDO("$drive:host=$host;dbname=$dbname", $user, $password);
    }

    function __destruct() {
        $this->$connection = null;
    }
}