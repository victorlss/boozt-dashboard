<?php

namespace Core;

class Configuration
{
    function __construct()
    {
        $this->bootstrapSelf();
    }

    private function bootstrapSelf()
    {
        //Load configuration file
        include(dirname(dirname(__DIR__)) . '/config.php');

        foreach($configurarion as $key => $value)
        {
            $this->{$key} = $value;
        }
    }
}