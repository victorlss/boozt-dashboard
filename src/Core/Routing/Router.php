<?php

namespace Core\Routing;
use Core\Configuration;
use Controllers;

class Router
{
    private $request;
    private $routes = array();
    private $supportedHttpMethods = array(
        "GET",
        "POST",
        "PUT",
        "DELETE"
    );

    function __construct(Request $request)
    {
        //Dependency Injection
        $this->request = $request;
    }

    function __destruct()
    {
        $this->resolve();
    }

    function __call($name, $args): void
    {   
        //Check if is a supported HTTP Method
        if(!in_array(strtoupper($name), $this->supportedHttpMethods))
        {
            $this->invalidMethodHandler();
            return;
        }

        array_push($this->routes, array(
            'method' => strtoupper($name),
            'uri' => $args[0],
            'params' => $args[1]
        ));
    }

    private function resolve()
    {
        foreach ($this->routes as $key => $value) {
            if($value['method'] === $this->request->requestMethod
            && $value['uri'] === $this->request->requestUri)
            {
                //Call action method on controller
                $controllerClass = 'Controllers\\' . $value['params']['controller'];
                $obj = new $controllerClass(new Configuration());
                $method = $value['params']['action'];
                call_user_func(array($obj, $method), $this->request->getBody());
                return;
            }
        }

        //If request route does not match with configured routes, then throws 404 status
        $this->defaultRequestHandler();
    }

    private function invalidMethodHandler()
    {
        header("{$this->request->serverProtocol} 405 Method Not Allowed");
    }

    private function defaultRequestHandler()
    {
        header("{$this->request->serverProtocol} 404 Not Found");
    }
} 