<?php

namespace Core\Routing;
use Core\Helper\Text;

class Request
{
    public $resolved = false;

    function __construct()
    {
        $this->bootstrapSelf();
    }

    private function bootstrapSelf()
    {
        foreach($_SERVER as $key => $value)
        {
            $this->{Text::toCamelCase($key)} = $value;
        }
    }
    
    public function getBody()
    {
        if($this->requestMethod === "GET")
        {
            return;
        }

        if ($this->requestMethod == "POST")
        {
            $result = array();
            foreach($_POST as $key => $value)
            {
                $result[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_SPECIAL_CHARS);
            }
            return $result;
        }
        
        return $body;
    }
}