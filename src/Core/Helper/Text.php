<?php

namespace Core\Helper;

class Text
{
    static function toCamelCase($string) : string
    {
        $result = strtolower($string);
            
        preg_match_all('/_[a-z]/', $result, $matches);
        foreach($matches[0] as $match)
        {
            $c = str_replace('_', '', strtoupper($match));
            $result = str_replace($match, $c, $result);
        }
        return $result;
    }
}