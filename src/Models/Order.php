<?php

namespace Models;

class Order
{
    public $id;
    public $purchase_date;
    public $device;
    public $country_code;
    public $customer_id;
} 