<?php

namespace DAL;
use PDO;

class Order implements DALInterface
{
    private $database;

    function __construct()
    {
        $this->database = new \Core\Database();
    }

    public function getAll()
    {    
        $sqlQuery = 'SELECT * FROM `order`'; 
        $stmt = $this->database->connection->prepare($sqlQuery);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS, '\Models\Order');
    }

    public function getById($id)
    {
        $sqlQuery = 'SELECT * FROM `order` WHERE id = :id';
        $stmt = $this->database->connection->prepare($sqlQuery);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        return $stmt->fetchObject('\Models\Order');
    }

    public function getCount()
    {   
        $sqlQuery = 'SELECT count(*) FROM `order`';
        $stmt = $this->database->connection->prepare($sqlQuery);
        $stmt->execute();
        return (int)$stmt->fetchColumn();
    }

    public function getCountDateByPeriod($startDate, $endDate)
    {   
        $sqlQuery = "SELECT DATE_FORMAT(purchase_date, '%b %e') as date, COUNT(*) as count
                    FROM `order`
                    WHERE purchase_date BETWEEN :startDate AND :endDate
                    group by purchase_date
                    order by purchase_date";

        $stmt = $this->database->connection->prepare($sqlQuery);
        $stmt->bindParam(':startDate', $startDate);
        $stmt->bindParam(':endDate', $endDate);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getCountByPeriod($startDate, $endDate)
    {   
        $sqlQuery = 'SELECT count(*) FROM `order` WHERE purchase_date BETWEEN :startDate AND :endDate';
        $stmt = $this->database->connection->prepare($sqlQuery);
        $stmt->bindParam(':startDate', $startDate);
        $stmt->bindParam(':endDate', $endDate);
        $stmt->execute();
        return (int)$stmt->fetchColumn();
    }

    public function getRevenueByPeriod($startDate, $endDate)
    {   
        $sqlQuery = 'SELECT COALESCE(SUM(i.price * i.price), 0)
                    FROM `order_item` i
                    JOIN `order` o ON o.id = i.order_id
                    WHERE purchase_date BETWEEN :startDate AND :endDate';

        $stmt = $this->database->connection->prepare($sqlQuery);
        $stmt->bindParam(':startDate', $startDate);
        $stmt->bindParam(':endDate', $endDate);
        $stmt->execute();
        return (double)$stmt->fetchColumn();
    }
}
