<?php

namespace DAL;

interface DALInterface
{
    public function getAll();
    public function getById($id);
}