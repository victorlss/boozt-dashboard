<?php

namespace DAL;
use PDO;

class Customer implements DALInterface
{
    private $database;

    function __construct()
    {
        $this->database = new \Core\Database();
    }

    public function getAll()
    {    
        $sqlQuery = 'SELECT * FROM `customer`'; 
        $stmt = $this->database->connection->prepare($sqlQuery);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS, '\Models\Customer');
    }

    public function getById($id)
    {
        $sqlQuery = 'SELECT * FROM `customer` WHERE id = :id';
        $stmt = $this->database->connection->prepare($sqlQuery);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        return $stmt->fetchObject('\Models\Customer');
    }

    public function getCount()
    {   
        $sqlQuery = 'SELECT count(*) FROM `customer`';
        $stmt = $this->database->connection->prepare($sqlQuery);
        $stmt->execute();
        return (int)$stmt->fetchColumn();
    }
}
