<div class="wrapper">
    <!-- sidebar -->
    <?php include($this->configuration->baseDir . "/src/Views/_Layout/Sidebar.php") ?>

    <!-- #content -->
    <div id="content">
        <h2>Dashboard</h2>      

        <h6 class="text-muted dashboard-dates">            
            <input id="startDate" type="text" class="text-muted"> to <input id="endDate" class="text-muted" type="text"/>
            <button id="btnRefresh" type="button" class="btn btn-link" onclick="javascript:loadData()" ><i class="fa fa-sync-alt"></i></button>
        </h6>

        <div class="line"></div>

        <div class="row">
            <!-- Total number of orders -->
            <div class="col-lg-4 col-sm-6">   
                <div class="card">
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col">
                                <h6 class="text-uppercase text-muted">Number of orders</h6>                    
                                <span id="numberOfOrders" class="h3"></span>
                            </div>
                            <div class="col-auto">
                                <span class="h2 fa fa-box text-muted"></span>
                            </div>                        
                        </div>
                    </div>
                </div>
            </div>
            <!-- Total number of revenue -->
            <div class="col-lg-4 col-sm-6">   
                <div class="card">
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col">
                                <h6 class="text-uppercase text-muted">Number of revenue</h6>                    
                                <span id="numberOfRevenue" class="h3"></span>
                            </div>
                            <div class="col-auto">
                                <span class="h2 fa fa-dollar-sign text-muted"></span>
                            </div>                        
                        </div>
                    </div>
                </div>
            </div>
            <!-- Total number of customers -->
            <div class="col-lg-4 col-sm-6">                    
                <div class="card">
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col">
                                <h6 class="text-uppercase text-muted">Number of customers</h6>                    
                                <span id="numberOfCustomer" class="h3"></span>
                            </div>
                            <div class="col-auto">
                                <span class="h2 fa fa-user text-muted"></span>
                            </div>                        
                        </div>
                    </div>
                </div>
            </div>         
        </div>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body" >
                        <h6 class="text-uppercase text-muted">Orders and Customers</h6>  
                        <div id="ordersCustumersChart"></div>
                    </div>
                </div>                    
            </div>            
        <div>
    </div><!-- /#content -->   
</div><!-- /.wrapper -->

<!-- jQuery CDN -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script>        
    
    //Start datepicker
    $('.dashboard-dates > input ').datepicker({
        format: 'yyyy-mm-dd'
    });

    //Set dates
    var date = new Date();
    $('#endDate').val(date.toISOString().slice(0, 10));
    date.setDate(date.getDate() - 30);
    $('#startDate').val(date.toISOString().slice(0, 10));
    
    loadData();
    renderChart();

    function loadData()
    {
        //Number of Orders
        $.ajax({
            url: '/Order/Count',
            dataType: "json",
            type: "POST",
            data: { 'startDate': $('#startDate').val(), 'endDate': $('#endDate').val() },
            success: function (data) {
                $('#numberOfOrders').html(data.count);
            }
        });

        //Number of Revenue
        $.ajax({
            url: '/Order/Revenue',
            dataType: "json",
            type: "POST",
            data: { 'startDate': $('#startDate').val(), 'endDate': $('#endDate').val() },
            success: function (data) {
                $('#numberOfRevenue').html('$' + data.revenue);
            }
        });

        //Number of Customers
        $.ajax({
            url: '/Customer/Count',
            dataType: "json",
            type: "POST",
            data: { 'startDate': $('#startDate').val(), 'endDate': $('#endDate').val() },
            success: function (data) {
                $('#numberOfCustomer').html(data.count);
            }
        });

        $.ajax({
            url: '/Order/DataChart',
            dataType: "json",
            type: "POST",
            data: { 'startDate': $('#startDate').val(), 'endDate': $('#endDate').val() },
            success: function (data) {
                renderChart(data.categories, data.count, data.customers);
            }
        });
    }
    
    function renderChart(categories, count, customers)
    {
        Highcharts.chart('ordersCustumersChart', {
            chart: {
                type: 'line'
            },  
            title: null,   
            xAxis: {
                categories: categories
            },
            yAxis: [{
                title: {
                    text: 'Orders'
                }
            },
            {
                title: {
                    text: 'Customers',
                },
                labels: {
                    format: '{value}'
                },
                opposite: true
            }],
            series: [{
                name: 'Orders',
                data: count
            },
            {
                name: 'Customers',
                yAxis: 1,
                data: customers
            }]
        });
    }
</script>