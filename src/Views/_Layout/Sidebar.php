<nav id="sidebar">
    <div class="sidebar-header">
        <h3>Boozt Test</h3>
    </div>
    <ul class="list-unstyled">
        <li>
            <a href="/">
                <i class="fas fa-tachometer-alt"></i>
                Dashboard
            </a>
        </li>
        <li>
            <a href="/About">
                <i class="fas fa-question"></i>
                About
            </a>
        </li>                
    </ul>
</nav>