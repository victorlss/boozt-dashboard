<div class="wrapper">
    <!-- sidebar -->
    <?php include($this->configuration->baseDir . "/src/Views/_Layout/Sidebar.php") ?>

    <!-- #content -->
    <div id="content">
        <h2>About</h2>      
        <div class="line"></div>
        <div class="row">
            <div class="col">
                Developed by Victor Souza da Silva.
            </div>
        </div>
    </div>
</div>