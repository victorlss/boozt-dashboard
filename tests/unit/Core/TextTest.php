<?php

use PHPUnit\Framework\TestCase;
use Core\Helper\Text;

final class TextTest extends TestCase
{
    public function testShouldConvertTextToCamelCase(): void
    {
        $this->assertEquals(
            'itIsCamelCase',
            Text::toCamelCase('it_is_camel_case')
        );
    }
}