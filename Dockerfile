FROM    php:7.3.0-apache
RUN     a2enmod rewrite \
        && docker-php-ext-install pdo_mysql
    